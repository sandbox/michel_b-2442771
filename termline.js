/*
 * Timeline JS script.
 *
 */
jQuery.noConflict();

(function($) {

  'use strict';

  Drupal.behaviors.timeline = {
    attach: function () {


        $(document).ready(function () {

            var tabletBreakpoint = 767,
            desktopBreakpoint = 940;


        // TIMELINE FUNCTIONS
        // ---------------------------------------------------------------------
        function updateButtons() {
          var previousBtnText, nextBtnText, currentSlide, previousSlide,
          nextSlide, numberOfSlides;

          numberOfSlides = $('.timeline_slides .timeline_slide').length;
          currentSlide = $('.timeline_slides').slickCurrentSlide();

          // Previous button
          previousSlide = $('.timeline_slides').find('.timeline_slide[index="'+(currentSlide - 1)+'"]');
          previousBtnText = $(previousSlide).find('.slide_title').text();
          $('.timeline_slides').find('.slick-prev').find('.button-text').text(previousBtnText);
          if ((currentSlide - 1) < 0) {
            $('.timeline_slides').find('.slick-prev').addClass('inactive').attr('tabindex', -1);
          } else {
            $('.timeline_slides').find('.slick-prev').removeClass('inactive').attr('tabindex', 0);
          }

          // Next button
          nextSlide = $('.timeline_slides').find('.timeline_slide[index="'+(currentSlide + 1)+'"]');
          nextBtnText = $(nextSlide).find('.slide_title').text();
          $('.timeline_slides').find('.slick-next').find('.button-text').text(nextBtnText);
          if ((currentSlide + 1) === numberOfSlides) {
            $('.timeline_slides').find('.slick-next').addClass('inactive').attr('tabindex', -1);
          } else {
            $('.timeline_slides').find('.slick-next').removeClass('inactive').attr('tabindex', 0);
          }
        }

        function updateSelectors() {
          var currentSlide = $('.timeline_slides').slickCurrentSlide();

          $( '.timeline_controls .timeline_control' ).each(function(index) {
            if (index === currentSlide) {
              $(this).find('.timeline_selector').addClass('selected');
              $(this).find('.timeline_selector').attr('aria-selected', 'true');
            } else {
              $(this).find('.timeline_selector').removeClass('selected');
              $(this).find('.timeline_selector').attr('aria-selected', 'false');
            }
          });
        }

        function disableSlides() {
          $( '.timeline_slides .timeline_slide' ).each(function() {
            if (!$(this).hasClass('slick-active')) {
              $(this).attr('aria-hidden', 'true');
              $(this).find('a').attr('tabindex', -1);
              $(this).find('button').attr('tabindex', -1);
            } else {
              $(this).attr('aria-hidden', 'false');
              $(this).find('a').attr('tabindex', 0);
              $(this).find('button').attr('tabindex', 0);
            }
          });
        }

        function updateScreenReaderText() {
          var slideLabel;
          slideLabel =  $( '.timeline_slides').find('.slick-active').find('.slide_title').text();
          $('.timeline').find('.current-slide-info').text(slideLabel);
        }

        // ENQUIRE FUNCTIONS
        // ---------------------------------------------------------------------

        enquire.register('screen and (max-width:' + (tabletBreakpoint-1) + 'px)', {
          match : function() {

            // T4 TIMELINE ACCORDION
            //------------------------------------------------------------------

            if ($('.timeline_slides').length !== 0) {
              $('.timeline_slides').v2hAccordion({
                verticalOnly: true,
                accordionClass: 'js-accordion',
                defaultOpened: 1,
                activeTabVisible: true,
                triggerClass: 'slide_title',
                sectionClass: 'timeline_slide_content-wrapper',
                destroyBreakpoint: 750
              });
            }
          },

          unmatch : function() {
            if ($('.related-content-inner').length !== 0) {
              $('.related-content-inner').data('plugin_hideShow').destroy();
            }

            if ($('.included-events').length !== 0) {
              $('.included-events .item-list').data('plugin_hideShow').destroy();
            }

            if ($('.filter').length !== 0) {
              $('.filter_inner').data('plugin_hideShow').destroy();
            }

            if ($('.timeline_slides').length !== 0) {
              $('.timeline_slides').data('plugin_v2hAccordion').destroy();
            }
          }
        });

        enquire.register('screen and (min-width: ' + tabletBreakpoint + 'px)', {
          match : function() {

            // TIMELINE CAROUSEL
            // -----------------------------------------------------------------

            if ($('.timeline').length !== 0) {
              $('.timeline_controls ol').slick({
                slide: '.timeline_control',
                slidesToShow: 5,
                infinite: false,
                prevArrow: '<button type="button" tabindex="-1" aria-hidden="true" class="slick-prev">Previous year</button>',
                nextArrow: '<button type="button" tabindex="-1" aria-hidden="true" class="slick-next">Next year</button>',
                responsive: [
                  {
                    breakpoint: desktopBreakpoint,
                    settings: {
                      slidesToShow: 3
                    }
                  }
                ]
              });
              $('.timeline_slides').append('<div class="timeline_slides_buttons"><div class="grid-container"></div></div>');
              $('.timeline_slides').unwrap();
              $('.timeline_slide_content-inner').addClass('grid-container');

              $('.timeline_slides').slick({
                slide: '.timeline_slide',
                asNavFor: '.timeline_controls',
                slidesToShow: 1,
                focusOnSelect: true,
                adaptiveHeight: true,
                infinite: false,
                draggable: false,
                appendArrows: $('.timeline_slides_buttons .grid-container'),
                prevArrow: '<button type="button" class="timeline_slides-btn slick-prev"><span class="element-invisible">Previous: </span><span class="button-text"></span></button>',
                nextArrow: '<button type="button" class="timeline_slides-btn slick-next"><span class="element-invisible">Next: </span><span class="button-text"></span></button>',
                onAfterChange: function(){
                  updateButtons();
                  updateSelectors();
                  disableSlides();
                  updateScreenReaderText();
                }
              });

              // Add buttons to each point in the timeline
              $('.timeline_controls ol').find('.timeline_control').append('<button class="timeline_selector">Show information</button>');
              $( '.timeline_controls .timeline_control' ).each(function(index) {
                var controlledSlide = 'slide-' + (index + 1);
                $(this).find('.timeline_selector').attr('aria-controls', controlledSlide);
                $(this).find('.timeline_selector').click(function() {
                  $('.timeline_slides').slickGoTo(index);
                });
              });
              updateButtons();
              updateSelectors();
              disableSlides();

              $('.timeline_slide').equalheights({
                target: 'spotlight'
              });
            }
          },

          unmatch : function() {
            if ($('.timeline_controls ol').hasClass('slick-initialized')) {
              $('.timeline_controls ol').unslick();
              $('.timeline_controls ol').find('.timeline_selector').remove();
            }

            if ($('.timeline_slides').hasClass('slick-initialized')) {
              $('.timeline_slides').unslick();
              $('.timeline_slides').find('.timeline_slides_buttons').remove();
              $('.timeline_slides').wrap('<div class="grid-container"></div>');
              $('.timeline_slide_content-inner').removeClass('grid-container');
              $('.timeline_slide').data('plugin_equalheights').destroy();
            }
          }
        });

				enquire.register('screen and (max-width:' + (desktopBreakpoint-1) + 'px)', {

					// OPTIONAL
					// If supplied, triggered when a media query matches.
					match : function() {
						if ($('.navigation_main-menu').length !== 0) {
							$('.navigation_main-menu').hideShow({
								buttonClass: 'js-show-hide_btn js-main-menu_btn',
								buttonExpandedClass: 'js-main-menu_btn--expanded',
								hideText: 'Close main menu',
								showText: 'Open main menu',
								state: 'hidden',
								insertTriggerLocation: '.header_controls',
								insertMethod: 'append'
							});
						}

						if ($('.navigation_secondary-menu').length !== 0) {
							$('.navigation_secondary-menu').hideShow({
								buttonClass: 'js-show-hide_btn  js-secondary-menu_btn',
								buttonExpandedClass: 'js-secondary-menu_btn--expanded',
								hideText: 'Menu',
								showText: 'Menu',
								state: 'hidden',
								insertTriggerLocation: '.navigation_secondary-menu-controls',
								insertMethod: 'append'
							});

              $('.navigation_secondary-menu').data('plugin_hideShow').destroy();
              $('.navigation_secondary-menu').data('plugin_hideShow').rebuild();
            }

            $('.navigation_main-menu').data('plugin_hideShow').destroy();
            $('.navigation_main-menu').data('plugin_hideShow').rebuild();

          }

        }); //end of enquire


     }); //end of documentready

    }
  };

})(jQuery);
